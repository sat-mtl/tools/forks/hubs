import React, { useState } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { CreatePropsFromData } from "../debug-panel/Prop.js";
import styles from "../debug-panel/CollapsiblePanel.scss";
import Draggable from 'react-draggable';

function collapse(evt) {
  evt.target.classList.toggle("collapsed");
  evt.target.parentElement?.parentElement?.nextSibling?.classList.toggle("collapsed");
  return evt.target.parentElement?.parentElement?.nextSibling?.classList.contains("collapsed");
}

function openLink(url) {
  const win = window.open(url, "_blank");
  win.focus();
}

export function DraggablePanel({
  title,
  border,
  row,
  wrap,
  grow,
  url,
  isRoot,
  children,
  data,
  collapsed,
  onCollapse,
  backgroundColor,
  clear,
  download
}) {
  const rootClassName = classNames(border ? styles.borderTile : styles.borderlessTile);
  const rootStyle = {
    position: 'fixed',
    zIndex: 99, // Render in front of "Join Room" menu
    left: 0,    // Improve expand/collapse by making element origin the top left
    top: 0,
    flexGrow: grow ? 1 : 0,
    backgroundColor
  };
  const contentClassName = classNames(
    isRoot
      ? `${styles.collapsibleContentRoot} ${(!!collapsed && "collapsed") || ""}`
      : `${styles.collapsibleContent} ${(!!collapsed && "collapsed") || ""}`
  );
  const buttonClassName = classNames(`${styles.collapseButton} ${(!!collapsed && "collapsed") || ""}`);
  const contentStyle = {
    flexFlow: (row ? "row " : "column ") + (wrap ? "wrap" : "nowrap")
  };
  const [isPanelLocked, setPanelLockState] = useState(false);
  const [isPanelCollapsed, setPanelCollapseState] = useState(collapsed);

  const [position, setPosition] = useState({
    x: window.APP.store.state.satellite.panelX ?? 0,
    y: window.APP.store.state.satellite.panelY ?? 0,
  });
  const onStop = (evt, elem) => {
    setPosition({ x: elem.x, y: elem.y });
    window.APP.store.update(
      { satellite: {
        panelX: elem.x,
        panelY: elem.y,
      }
    });
  };

  return (
    <Draggable
      handle={`div.draggable-${title.displayName}`}
      disabled={isPanelLocked}
      onStop={onStop}
      position={position}
    >
    <div className={rootClassName} style={rootStyle}>
      <div className={styles.collapsibleHeader}>
        <div style={{ display: 'flex', flexDirection: 'row', gap: '1rem', color: 'white' }}>
          <div className={`draggable-${title.displayName}`} style={{ cursor: isPanelLocked ? 'auto' : 'pointer' }}>
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 45.9 45.9" xmlSpace="preserve" style={{ height: '1.5rem', width: '1.5rem' }}>
              <path fill="white" d="m44.4 24.55 1.5-1.51-1.5-1.5-5.34-5.34a2.13 2.13 0 0 0-3.02 3.01l1.7 1.7h-5.52a2.16 2.16 0 0 0-.85-.18h-6.2V8.17l1.34 1.33a2.13 2.13 0 0 0 3.02-3.01L24.55 1.5 23.04 0l-1.5 1.5-.01.01-5.33 5.33a2.13 2.13 0 1 0 3.02 3.02l1.69-1.7v5.52c-.12.26-.18.55-.18.85v6.2H8.17l1.34-1.34a2.13 2.13 0 0 0-3.02-3.01L1.5 21.35 0 22.87l1.5 1.5v.01l5.33 5.33a2.13 2.13 0 1 0 3.02-3.01L8.15 25h5.52c.26.12.55.18.85.18h6.2v12.57l-1.34-1.34a2.13 2.13 0 1 0-3.01 3.02l4.97 4.97 1.51 1.51 1.51-1.5 5.33-5.34a2.13 2.13 0 0 0-3.01-3.02l-1.7 1.7v-5.52a2 2 0 0 0 .19-.85v-6.2h12.56l-1.34 1.34a2.14 2.14 0 0 0 3.02 3.02l4.98-4.98z"></path>
            </svg>
          </div>
          <div
            style={{ cursor: 'pointer' }}
            onClick={() => {
                setPanelLockState(!isPanelLocked);
            }}
          >
            {
              isPanelLocked
                ? <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 330" xmlSpace="preserve" style={{ height: '1.5rem', width: '1.5rem' }}>
                    <path fill="red" d="M65 330h200a15 15 0 0 0 15-15V145a15 15 0 0 0-15-15h-15V85c0-46.87-38.13-85-85-85S80 38.13 80 85v45H65a15 15 0 0 0-15 15v170a15 15 0 0 0 15 15zm115-95.01V255a15 15 0 0 1-30 0v-20.01A24.97 24.97 0 0 1 140 215c0-13.79 11.21-25 25-25s25 11.21 25 25c0 8.16-3.93 15.42-10 19.99zM110 85c0-30.33 24.67-55 55-55s55 24.67 55 55v45H110V85z"/>
                  </svg>
                : <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 330 330" xmlSpace="preserve" style={{ height: '1.5rem', width: '1.5rem' }}>
                    <path fill="white" d="M15 160a15 15 0 0 0 15-15V85c0-30.33 24.67-55 55-55s55 24.67 55 55v45h-25a15 15 0 0 0-15 15v170a15 15 0 0 0 15 15h200a15 15 0 0 0 15-15V145a15 15 0 0 0-15-15H170V85c0-46.87-38.13-85-85-85S0 38.13 0 85v60a15 15 0 0 0 15 15z"></path>
                  </svg>
            }
          </div>
          <div
            style={{ height: '1.5rem', width: '1.5rem', fontSize: '1.5rem', cursor: 'pointer' }}
            onClick={() => {
              setPanelCollapseState(!isPanelCollapsed);
            }}
          >
            { isPanelCollapsed ? '+' : '-'}
          </div>
        </div>
        {title && (
            <div style={{ marginLeft: '1rem', color: 'white', fontSize: '1.5rem' }}>{title}</div>
        )}
        {url && (
          <button className={classNames(styles.logButton)} onClick={() => openLink(url)}>
            ?
          </button>
        )}
        {(clear || download) && (
          <div className={styles.collapsibleRightButtons}>
            {download && (
              <button className={classNames(styles.logButton)} onClick={download}>
                &#x2193;
              </button>
            )}
            {clear && (
              <button className={classNames(styles.logButton)} onClick={clear}>
                &#xd7;
              </button>
            )}
          </div>
        )}
      </div>
      <div className={contentClassName} style={{ display: isPanelCollapsed ? 'none' : 'flex', ...contentStyle }}>
        {CreatePropsFromData(data)}
        {children}
      </div>
    </div>
    </Draggable>
  );
}

DraggablePanel.propTypes = {
  children: PropTypes.node,
  title: PropTypes.node,
  border: PropTypes.bool,
  row: PropTypes.bool,
  grow: PropTypes.bool,
  wrap: PropTypes.bool,
  url: PropTypes.string,
  isRoot: PropTypes.bool,
  data: PropTypes.object,
  collapsed: PropTypes.bool,
  onCollapse: PropTypes.func,
  backgroundColor: PropTypes.string,
  clear: PropTypes.func,
  download: PropTypes.func
};
