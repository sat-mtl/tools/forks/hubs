import { paths } from "../paths";
import { sets } from "../sets";
import { xforms } from "./xforms";
import { addSetsToBindings } from "./utils";
import { bindingsData } from "./mediapipe-bindings-data";
import { mediaPipeActionPrefs } from "./mediapipe-bindings-data";

function generateBindings(bindingsKey, bindingsSrc) {
  return bindingsData[bindingsKey].bindings.map(e => {
    if ('value' in e.src) {
      e.src.value = bindingsSrc;
    } else {
      e.src.first = bindingsSrc;
    }
    return e;
  });
}

export function updateMediaPipeUserBindings(currentBindings, dValue, dSrc) {
  const newBindings = currentBindings;

  // Remove and re-add lobby camera, because it must be after updates/changes to cameraDelta
  newBindings[sets.global] = currentBindings[sets.global].filter(elem => elem.dest?.value !== paths.actions.lobbyCameraDelta);

  // Remove the existing entry for this gesture
  newBindings[sets.global] = currentBindings[sets.global].filter(elem => elem.src?.value !== dSrc);

  const actions = generateBindings(dValue, dSrc);
  newBindings[sets.global] = newBindings[sets.global].concat(actions);

  let found_look_actions = [];
  for (const binding of newBindings[sets.global]) {
    if (binding.src?.value && [
      paths.device.mediapipe.isLookingUp,
      paths.device.mediapipe.isLookingDown,
      paths.device.mediapipe.isLookingLeft,
      paths.device.mediapipe.isLookingRight,
    ].includes(binding.src.value)) {
      found_look_actions.push(binding.dest.value);
    }
  }

  // IF we did not just change an action to one of the look direction actions
  // then we remove that action's existing entry
  // ELSE we add that binding object back in for the selected action
  if (!['lookUp', 'lookDown', 'lookLeft', 'lookRight'].includes(dValue)) {
    const actions_to_remove = [
      paths.actions.mediapipe.lookUp,
      paths.actions.mediapipe.lookDown,
      paths.actions.mediapipe.lookLeft,
      paths.actions.mediapipe.lookRight,
    ].filter(action => !found_look_actions.includes(action));
    for (const found of actions_to_remove) {
      newBindings[sets.global] = newBindings[sets.global].filter(elem => elem.src?.first !== found);
    }
  } else {
    switch(`${dValue}`) {
      case "lookUp":
        newBindings[sets.global].push(
        {
          src: {
            first: paths.actions.mediapipe.lookUp,
            second: paths.actions.cameraDelta,
          },
          dest: { value: paths.actions.cameraDelta },
          xform: xforms.add_vec2
        });
        break;
      case "lookDown":
        newBindings[sets.global].push(
        {
          src: {
            first: paths.actions.mediapipe.lookDown,
            second: paths.actions.cameraDelta,
          },
          dest: { value: paths.actions.cameraDelta },
          xform: xforms.add_vec2
        });
        break;
      case "lookLeft":
        newBindings[sets.global].push(
        {
          src: {
            first: paths.actions.mediapipe.lookLeft,
            second: paths.actions.cameraDelta,
          },
          dest: { value: paths.actions.cameraDelta },
          xform: xforms.add_vec2
        });
        break;
      case "lookRight":
        newBindings[sets.global].push(
        {
          src: {
            first: paths.actions.mediapipe.lookRight,
            second: paths.actions.cameraDelta,
          },
          dest: { value: paths.actions.cameraDelta },
          xform: xforms.add_vec2
        });
        break;
    }
  }

  newBindings[sets.global].push(
  {
    src: { value: paths.actions.cameraDelta },
    dest: { value: paths.actions.lobbyCameraDelta },
    xform: xforms.copy
  });

  return addSetsToBindings(newBindings);
}
export function mediaPipeUserBindings(preferences) {
  const faceUpPref    = preferences[mediaPipeActionPrefs.faceUp.key];
  const faceDownPref  = preferences[mediaPipeActionPrefs.faceDown.key];
  const faceLeftPref  = preferences[mediaPipeActionPrefs.faceLeft.key];
  const faceRightPref = preferences[mediaPipeActionPrefs.faceRight.key];
  const leftEyePref   = preferences[mediaPipeActionPrefs.leftEye.key];
  const rightEyePref  = preferences[mediaPipeActionPrefs.rightEye.key];
  const mouthPref     = preferences[mediaPipeActionPrefs.mouth.key];
  const BINDINGS = {
    [sets.satelliteSAT]: [],
    [sets.global]: [
      {
        src:  { value: paths.device.mediapipe.lookY },
        dest: { value: paths.device.mediapipe.isLookingUp },
        xform: xforms.true_if_positive
      },
      {
        src:  { value: paths.device.mediapipe.lookY },
        dest: { value: paths.device.mediapipe.isLookingDown },
        xform: xforms.true_if_negative
      },
      {
        src:  { value: paths.device.mediapipe.lookX },
        dest: { value: paths.device.mediapipe.isLookingLeft },
        xform: xforms.true_if_negative
      },
      {
        src:  { value: paths.device.mediapipe.lookX },
        dest: { value: paths.device.mediapipe.isLookingRight },
        xform: xforms.true_if_positive
      },
      {
        src: { value: paths.device.mediapipe.isLookingUp },
        dest: { value: paths.actions.mediapipe.lookUp },
        xform: xforms.set_vec2_y_to_1_if_true_otherwise_do_nothing
      },
      {
        src: { value: paths.device.mediapipe.isLookingDown },
        dest: { value: paths.actions.mediapipe.lookDown },
        xform: xforms.set_vec2_y_to_negative_1_if_true_otherwise_do_nothing
      },
      {
        src: { value: paths.device.mediapipe.isLookingLeft },
        dest: { value: paths.actions.mediapipe.lookLeft },
        xform: xforms.set_vec2_x_to_negative_1_if_true_otherwise_do_nothing
      },
      {
        src: { value: paths.device.mediapipe.isLookingRight  },
        dest: { value: paths.actions.mediapipe.lookRight },
        xform: xforms.set_vec2_x_to_1_if_true_otherwise_do_nothing
      },
      ...generateBindings(faceUpPref,    paths.actions.mediapipe[faceUpPref]),
      ...generateBindings(faceDownPref,  paths.actions.mediapipe[faceDownPref]),
      ...generateBindings(faceLeftPref,  paths.actions.mediapipe[faceLeftPref]),
      ...generateBindings(faceRightPref, paths.actions.mediapipe[faceRightPref]),
      ...generateBindings(leftEyePref,   paths.device.mediapipe.leftEye),
      ...generateBindings(rightEyePref,  paths.device.mediapipe.rightEye),
      ...generateBindings(mouthPref,     paths.device.mediapipe.mouth),
      {
        src: { value: paths.actions.cameraDelta },
        dest: { value: paths.actions.lobbyCameraDelta },
        xform: xforms.copy
      },
    ],
  };

  return addSetsToBindings(BINDINGS);
}
